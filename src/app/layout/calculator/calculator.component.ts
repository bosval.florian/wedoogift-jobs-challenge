import { CalculatorSearchResponse } from './../../core/model/calculator-search-response.model';
import { CalculatorComponentValue } from './../../core/model/calculator-component-value.model';
import { Component, forwardRef, OnInit } from '@angular/core';
import {
  ControlValueAccessor,
  FormBuilder,
  FormControl,
  FormGroup,
  NG_VALUE_ACCESSOR,
  Validators,
} from '@angular/forms';
import { CalculatorService } from 'src/app/core/service/calculator.service';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CalculatorComponent),
      multi: true,
    },
  ],
})
export class CalculatorComponent implements OnInit, ControlValueAccessor {
  public form: FormGroup;
  private calculatorValue: CalculatorComponentValue;
  public calculatorResponse: CalculatorSearchResponse;
  private shopId = 5;

  public value: FormControl = new FormControl(0, {
    validators: [Validators.required],
    updateOn: 'blur',
  });

  constructor(
    private formBuilder: FormBuilder,
    private calculatorService: CalculatorService
  ) {}

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      value: this.value,
    });
    this.form.get('value').valueChanges.subscribe((value: number) => {
      this.calculatorService
        .searchCombination(5, value)
        .subscribe((response: CalculatorSearchResponse) => {
          this.calculatorResponse = response;
          if (this.calculatorResponse.floor && !this.calculatorResponse.ceil) {
            this.form
              .get('value')
              .setValue(this.calculatorResponse.floor.value);
          } else if (
            !this.calculatorResponse.floor &&
            this.calculatorResponse.ceil
          ) {
            this.form.get('value').setValue(this.calculatorResponse.ceil.value);
          }
        });
    });
  }
  private onChange = (val: CalculatorComponentValue) => {};
  private onTouched = () => {};
  writeValue(val: CalculatorComponentValue): void {
    this.calculatorValue = val;
    this.onChange(val);
  }
  registerOnChange(fn: (val: CalculatorComponentValue) => void): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }

  public setValue(val: number) {
    this.form.get('value').setValue(val);
  }

  public nextValue() {
    this.calculatorService
      .searchCombination(5, this.value.value + 1)
      .subscribe((response: CalculatorSearchResponse) => {
        if (response.ceil) {
          this.calculatorResponse = response;
          this.form.get('value').setValue(this.calculatorResponse.ceil.value);
        }
      });
  }

  public prevValue() {
    this.calculatorService
      .searchCombination(5, this.value.value + 1)
      .subscribe((response: CalculatorSearchResponse) => {
        if (response.floor) {
          this.calculatorResponse = response;
          this.form.get('value').setValue(this.calculatorResponse.floor.value);
        }
      });
  }
  submit() {
    if (this.calculatorResponse.equal) {
      this.writeValue(this.calculatorResponse.equal);
    }
  }
}
