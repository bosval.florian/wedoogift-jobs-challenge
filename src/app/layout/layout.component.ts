import { CalculatorComponentValue } from './../core/model/calculator-component-value.model';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
})
export class LayoutComponent implements OnInit {
  public form: FormGroup;
  private calculatorValue: CalculatorComponentValue;
  constructor(private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.form = this.formBuilder.group({
      calculatorValue: this.calculatorValue,
    });
  }
}
