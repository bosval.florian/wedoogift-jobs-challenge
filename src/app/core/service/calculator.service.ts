import { CalculatorSearchResponse } from './../model/calculator-search-response.model';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class CalculatorService {
  constructor(protected http: HttpClient) {}

  public searchCombination(
    shopId: number,
    amount: number
  ): Observable<CalculatorSearchResponse> {
    return this.http.get<CalculatorSearchResponse>(
      'http://localhost:3000/shop/' +
        shopId +
        '/search-combination?amount=' +
        amount
    );
  }
}
