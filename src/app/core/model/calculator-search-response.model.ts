import { CalculatorComponentValue } from './calculator-component-value.model';
export interface CalculatorSearchResponse {
  equal: CalculatorComponentValue;
  floor: CalculatorComponentValue;
  ceil: CalculatorComponentValue;
}
